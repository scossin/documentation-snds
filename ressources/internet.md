# Ailleurs sur internet
<!-- SPDX-License-Identifier: MPL-2.0 -->

Ressource sur le SNDS disponibles sur d'autres sites internet.

- Le site [snds.gouv](https://www.snds.gouv.fr/SNDS/Accueil) 
pour des informations générales.

- Le [site de l'ATIH](https://www.atih.sante.fr)
pour des détails sur le PMSI
    - partie [information médicale](https://www.atih.sante.fr/domaines-d-activites/information-medicale) du site
    - plateforme de restitution des données des établissements de santé [scansanté](https://www.scansante.fr) 

- Le [site du CépiDc](https://cepidc.inserm.fr/causes-medicales-de-deces/la-base-des-causes-medicales-de-deces)
pour des détails sur la base médicale des causes de décès.

- Les [publications](https://www.irdes.fr/recherche/publications.html) de l'[Irdes](../glossaire/Irdes.md), dont certaines concernent le SNDS

- Le site open data du gouvernement : [data.gouv](https://www.data.gouv.fr) et la [partie dédiée à la santé](https://www.data.gouv.fr/fr/topics/sante-et-social/) avec notamment sur le profil de [l'assurance maladie](https://www.data.gouv.fr/fr/datasets/?q=assurance+maladie&organization=534fff5ca3a7292c64a77d1a) qui répértorie tous les jeux de données open data de la Cnam. 

- [Rubrique statistiques et publications](https://www.ameli.fr/l-assurance-maladie/statistiques-et-publications/index.php) sur le site Ameli de la Cnam.