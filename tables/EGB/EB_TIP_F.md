# EB_TIP_F

Table des données de codage de la Liste des Produits et Prestations


## Modèle de données

|Nom|Type|Description|Exemple|Propriétés|
|-|-|-|-|-|
|FLX_DIS_DTD|date|Date de mise à disposition des données|||
|LPP_ECT_MNT|nombre réel|Montant total de l'écart indemnisable LPP|||
|REM_TYP_AFF|nombre réel|type de remboursement affiné|||
|ORG_CLE_NUM|chaîne de caractères|organisme de liquidation des prestations (avant fusion des caisses)|||
|TIP_PRS_IDE|nombre réel|Code LPP du produit ou de la Prestations|||
|ORG_CLE_NEW|chaîne de caractères|Code de l'organisme de liquidation|||
|FLX_TRT_DTD|date|Date d'entrée des données dans le système d'information|||
|TIP_ORD_NUM|nombre réel|N° Ordre Prestation Affinee LPP|||
|DCT_ORD_NUM|nombre réel|numéro d'ordre du décompte dans l'organisme|||
|TIP_ACT_QSN|nombre réel|Quantité d'actes LPP|||
|TIP_ACL_DTD|date|Date de début de location ou d'achat|||
|LPP_ECU_MNT|nombre réel|Montant unitaire de l'écart indemnisable LPP|||
|TIP_SIR_NUM|chaîne de caractères|N° SIRET Fabriquant-Importateur|||
|FLX_EMT_TYP|nombre réel|Type d'émetteur|||
|CLE_TEC_PRS|chaîne de caractères|Clé technique Prestation|||
|TIP_PRS_TYP|nombre réel|Type de Prestations fournie|||
|TIP_ACT_PRU|nombre réel|Prix unitaire LPP du produit ou de la Prestations|||
|TIP_ACL_DTF|date|Date de fin de location ou d'achat|||
|FLX_EMT_NUM|nombre réel|numéro d'émetteur du flux|||
|PRS_ORD_NUM|nombre réel|Numéro d'ordre de la prestation dans le décompte|||
|FLX_EMT_ORD|nombre réel|numéro de séquence du flux|||
|TIP_PUB_PRX|nombre réel|Prix unitaire public|||
